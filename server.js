const mysql = require("mysql");
const express = require("express");
const bodyParser = require("body-parser");



const app = express();
app.use(bodyParser.json());
const port = 5050;
app.listen(port, () => {
  console.log("Server online on: " + port);
});
app.use("/", express.static("../front-end"));
const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "siscit_back_end",
});
connection.connect(function (err) {
  console.log("Connected to database!");
  const sql =
    "CREATE TABLE IF NOT EXISTS abonamente_metrou(nume VARCHAR(255), prenume  VARCHAR(255), email  VARCHAR(255), telefon  VARCHAR(255), cnp VARCHAR(20), varsta VARCHAR(3), gen VARCHAR(1), data_inceput  VARCHAR(10), data_sfarsit  VARCHAR(10))";
  connection.query(sql, function (err, result) {
    if (err) throw err;
  });
});
app.post("/bilet", (req, res) => {
  const bilet = {
    nume: req.body.nume,
    prenume: req.body.prenume,
    telefon: req.body.telefon,
    cnp: req.body.cnp,
    gen: req.body.gen,
    email: req.body.email,
    data_inceput: req.body.data_inceput,
    data_sfarsit: req.body.data_sfarsit,
    varsta: req.body.varsta
  };
  let error = [];
 


  if (!bilet.nume || !bilet.prenume || !bilet.telefon || !bilet.email || !bilet.cnp || !bilet.varsta || !bilet.data_inceput || !bilet.data_sfarsit) {
    error.push("Unul sau mai multe campuri nu au fost introduse");
    console.log("Unul sau mai multe campuri nu au fost introduse!");
  } else {
  if (bilet.nume.length < 3 && bilet.nume.length > 30) {
    console.log("Nume invalid!");
    error.push("Nume invalid");
  } else if (!bilet.nume.match("^[A-Za-z]+$")) {
    console.log("Numele trebuie sa contina doar litere!");
    error.push("Numele trebuie sa contina doar litere!");
  }

  if (bilet.prenume.length < 3 || bilet.nume.length > 30) {
    console.log("Prenume invalid!");
    error.push("Prenume invalid!");
  } else if (!bilet.prenume.match("^[A-Za-z]+$")) {
    console.log("Prenumele trebuie sa contina doar litere!");
    error.push("Prenumele trebuie sa contina doar litere!");
  }

  if (bilet.telefon.length != 10) {
      console.log("Numarul de telefon trebuie sa fie de 10 cifre!");
      error.push("Numarul de telefon trebuie sa fie de 10 cifre!");
  } else if (!bilet.telefon.match("^[0-9]+$")) {
      console.log("Numarul de telefon trebuie sa contina doar cifre!");
      error.push("Numarul de telefon trebuie sa contina doar cifre!");   
  }

  if (!bilet.email.includes("@gmail.com") && !bilet.email.includes("@yahoo.com")) {
    console.log("Email invalid!");
    error.push("Email invalid!");
  } 
  

  // validateEmail = check('email'); 
  // if ma bate :(
  // am vrut sa folosesc modulul "validate", dar nu m-am descurcat deoarece cred ca era nevoie si de un middleware
  
  
  if (bilet.data_inceput == new Date("DD/MM/YYYY") && bilet.data_inceput < bilet.data_sfarsit) {
    console.log("Formatul datei este invalid!");
    error.push("Formatul datei este invalid!");
  }

  if (bilet.data_sfarsit == new Date("DD/MM/YYYY")) {
      console.log("Formatul datei este invalid!");
      error.push("Formatul datei este invalid!");
  }

  if (bilet.validitate == bilet.data_inceput <= bilet.validitate <= bilet.data_sfarsit) {
    console.log("Ziua curenta nu se afla intre datele de inceput si de sfarsit!");
    error.push("Ziua curenta nu se afla intre datele de inceput si de sfarsit!");
  }

  if (bilet.cnp.length != 13) {
    console.log("CNP invalid!");
    error.push("CNP invalid!");
  } else if (!bilet.cnp.match("^[0-9]+$")) {
    console.log("CNP-ul trebuie sa contina doar cifre!");
    error.push("CNP-ul trebuie sa contina doar cifre!");
  }


  // trebuie folosita operatie CNP / 10000000000) % 100 pentru a afla a doua si a treia cifra, dar nu stiu cum sa implementez corelarea anului cu varsta. 
  // m-am gandit ca trebuie folosit modulul "moment" pentru asta, dar am esuat in a-l implementa... :(


  if (((bilet.cnp / 1000000000000) % 100) == 1 || ((bilet.cnp / 1000000000000) % 100) == 3 || ((bilet.cnp / 1000000000000) % 100) == 5) 
    bilet.gen = 'M';
    else if (((bilet.cnp / 1000000000000) % 100) == 2 || ((bilet.cnp / 1000000000000) % 100) == 4 || ((bilet.cnp / 1000000000000) % 100) == 6) 
    bilet.gen = 'F';

  if (bilet.varsta.length < 1 && bilet.varsta.length > 3) {
    console.log("Varsta invalida!");
    error.push("Varsta invalida");
  } else if (!bilet.varsta.match("^[0-9]+$")) {
    console.log("Varsta trebuie sa contina doar cifre!");
    error.push("Varsta trebuie sa contina doar cifre!");
  }
}

if (error.length === 0) {
    const sql = `INSERT INTO abonamente_metrou 
    (
      nume,
      prenume,
      telefon,
      cnp,
      gen,
      email,
      data_inceput,
      data_sfarsit,
      varsta
    ) 
      VALUES (?,?,?,?,?,?,?,?,?)`;
    connection.query(
      sql,
      [
        bilet.nume,
        bilet.prenume,
        bilet.telefon,
        bilet.cnp,
        bilet.gen,
        bilet.email,
        bilet.data_inceput,
        bilet.data_sfarsit,
        bilet.varsta,
      ],
      function (err, result) {
        if (err) throw err;
        console.log("Abonament realizat cu succes!");
        res.status(200).send({
          message: "Abonament realizat cu succes!",
        });
        console.log(sql);
      }
    );
  } else {
    res.status(500).send(error);
    console.log("Abonamentul nu a putut fi creat!");
  }
  app.use('/', express.static('../front-end'))
});
